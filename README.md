# Révisions Partie 1

Ce document résume le cours d'AEO sur les points importants, avec des exemples et des explications.

### Les librairies

```VHDL
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
-- (use ieee.std_logic_arith.all;)
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
```

STD_LOGIC_UNSIGNED sert pour effectuer une addition entre deux STD_LOGIC_VECTOR.

        SUM <= NUM1 + NUM2;

use ieee.std_logic_arith.all;
sert à utiliser les opérations d'addition sur des std_ulogic_vector.

        SUM_U <= unsigned(A) + unsigned(B).

### Les entitités

```VHDL
entity my_add is
    Port ( sw : in STD_LOGIC_VECTOR (7 downto 0);
           led : out STD_LOGIC_VECTOR (4 downto 0);
           dp : out STD_LOGIC;
           an : out STD_LOGIC_VECTOR (3 downto 0) := "1110");
end my_add;
```

Une entité possède un nom, des valeurs d'entrée et des valeurs de sorties, 
elles peuvent être executer dans des instances d'autres entités.

Généralement, les types entrants et sortant sont des STD_LOGIC qui sont des valeurs binaires, 
ou des STD_LOGIC_VECTOR (B downto A) qui sont des sortes de tableaux de binaires

### L'architecture

```VHDL
architecture Behavioral of my_add is
    signal sl : std_logic_vector(3 downto 0);
    signal sr : std_logic_vector(3 downto 0);
    signal sum : std_logic_vector(4 downto 0);
```

L'architecture contient les signaux. Elle se place avant le "begin"

### L'exécution

```VHDL
begin 
    sl<=sw(3 downto 0);
    sr<=sw(7 downto 4)
    sum <= ('0' & sl) + ('0' & sr);
    led <= sum
    dp <= not sum(4);
end Behavioral;
```

Ici le code sépare les parties droite et gauche du switch,
fait la somme des deux,
l'affiche sur la led,
et affiche la retenu sur dp s'il y en a une.

### With select

```VHDL
entity x7seg is
    Port ( sw : in STD_LOGIC_VECTOR (3 downto 0);
           seg : out STD_LOGIC_VECTOR (6 downto 0));
end x7seg;

architecture exo7seg of x7seg is

begin

    with sw select 
        seg <= "1000000" when "0000", -- 0
               "1111001" when "0001", -- 1
               "0100100" when "0010", -- 2
               "0110000" when "0011", -- 3
               "0011001" when "0100", -- 4
               "0010010" when "0101", -- 5
               "0000010" when "0110", -- 6
               "1111000" when "0111", -- 7
               "0000000" when "1000", -- 8
               "0010000" when "1001", -- 9
               "0001000" when "1010", -- A
               "0000011" when "1011", -- b
               "1000110" when "1100", -- C
               "0100001" when "1101", -- d
               "0000110" when "1110", -- E
               "0001110" when "1111", -- F
               "1111111" when others;

end exo7seg;
```

Le with select permet de renvoyer une valeur en fonction de la valeur d'une autre.
Cela peut s'apparenté à un switch en Java au niveau de la syntaxe.


Ce code permet d'afficher des valeurs hexadécimales en fonction de la valeur des switchs. 
Pour allumer un segment on envoie 0, sinon on envoie 1.
L'ordre d'allumage des 7 segments vaut G-F-E-D-C-B-A.

![Logo Responsive Mind](https://sites.google.com/site/l2s3ae/_/rsrc/1390230881145/home/lecon-1/fig18.png "Afficheur 7 segments")

On peut remarquer que pour allumer un chiffre, par exemple celui à droite,
Il faut allumer l'anode "an".

      an : out STD_LOGIC_VECTOR (3 downto 0) := "1110";


### Les instances et composants

```VHDL
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity calc is
    Port ( sw : in STD_LOGIC_VECTOR (15 downto 0);
           led : out STD_LOGIC_VECTOR (15 downto 0);
           seg : out STD_LOGIC_VECTOR (6 downto 0);
           an : out STD_LOGIC_VECTOR (3 downto 0) := "1110";
           btnL : in STD_LOGIC;
           btnR : in STD_LOGIC);
end calc;

architecture Behavioral of calc is

COMPONENT add4
    Port ( sl : in STD_LOGIC_VECTOR (3 downto 0);
           sr : in STD_LOGIC_VECTOR (3 downto 0);
           led : out STD_LOGIC_VECTOR (4 downto 0));
END COMPONENT;

COMPONENT x7seg
    Port ( sw : in STD_LOGIC_VECTOR (3 downto 0);
           seg : out STD_LOGIC_VECTOR (6 downto 0));
END COMPONENT;

signal res_add4 : STD_LOGIC_VECTOR (15 downto 0);

begin

Inst_add4: add4 PORT MAP(
    sl => sw(3 downto 0),sr => sw(7 downto 4),led(4 downto 0) => res_add4(4 downto 0));

res_btn <= btnL&btnR;

-- Nous n'avons pas ajouté les instances res_and4, res_or4, res_xor4 par soucis de place
-- Mais le code est le même que pour add4.
 with (res_btn) select
     res<= res_add4 when "00",
         res_and4 when "01",
         res_or4 when "10",
         res_xor4 when others;
         
led<=res;         
         
Inst_x7seg: x7seg PORT MAP(
  sw => res(3 downto 0),seg => seg);

end Behavioral;
```

Les instances d'entités doivent être remplit avec des valeurs, et seront calculées à l'exécution du programme.
- Pour les ports d'entrée, on fait valeur_quon_veut_ajouter => valeur_du_composant.
- Pour les valeurs de sortie, on fait valeur_du_composant => valeur_quon_veut_recuperer.

Ces entités utilise des composants que nous devons définir. Nous précisons donc les nommages utilisées de nos entités.

Attention, il est nécéssaire d'utiliser des signaux pour les valeurs de sortie des instances.
Il sera donc nécésaire de définir à la fin vers quel port de sortie devra aller le signal calculé par l'instance.


### Les opérateurs 

Pour réaliser une calculatrice, nous pouvons utiliser les opérateurs suivants :

```
- +
- AND
- OR
- XOR
```

### Les processus

```VHDL
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_arith.all;

entity parity is
    Port ( sw : in STD_LOGIC_VECTOR (15 downto 0);
          res_parity : out STD_LOGIC_VECTOR (6 downto 0));
end parity;

architecture Behavioral of parity is

begin
    PROCESS(sw)
    variable N : std_logic;
    begin     
        N := '0';  
        for i in 0 to 15 loop
            N := N XOR sw(i);
        end loop;  
        if N = '1' then
            res_parity <= "1000000";
        end if;
        if N = '0' then
            res_parity <= "1100000";
        end if;
            
    end process;

end Behavioral;
```

Les processus servent à réaliser des programmes avec des variables.
Ils permettent de faire des boucles for, des if, des else...

Les variables sont toujours à définir avant le "begin" du processus.
Le processus doit définir ce dont il a besoin, dans cet exemple il 
utilise PROCESS(sw).

### La clock

```VHDL
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity chenillard is
  Port (  sw : in STD_LOGIC_VECTOR (15 downto 0);
          clk : in STD_LOGIC;
          led : out STD_LOGIC_VECTOR (15 downto 0) );
end chenillard;

architecture Behavioral of chenillard is

COMPONENT clk_div
 Port (clk : in STD_LOGIC;
      clk_4hz : out STD_LOGIC);
END COMPONENT;

COMPONENT shift_vector
  Port (sw : in STD_LOGIC_VECTOR (15 downto 0);
      clk : in STD_LOGIC;
      led : out STD_LOGIC_VECTOR (15 downto 0) );
END COMPONENT;

signal clk_4hz : STD_LOGIC;

begin
    
Inst_clk_div: clk_div PORT MAP(
  clk=>clk,clk_4hz=>clk_4hz);
        
Inst_shift_vector: shift_vector PORT MAP(
    sw=> sw, led=>led, clk=>clk_4hz);
    
end Behavioral;
```

```VHDL
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

entity clk_div is
 Port (clk : in STD_LOGIC;
       clk_4hz : out STD_LOGIC);
end clk_div;

architecture Behavioral of clk_div is

signal clk_temp : STD_LOGIC := '0';
begin
process (clk) 
    -- declaration d un compteur sur 24 bits
    variable counter : unsigned(23 downto 0):= (others => '0');
    variable aux : STD_LOGIC;
    begin 
    
    if (clk'event and clk='1') then
        counter:=counter+1;
        if (counter=X"BEBC20") then
            clk_temp <= not clk_temp;
            counter := (others => '0');
            
        end if;
        
    end if;                 
end process;

clk_4hz<= clk_temp;

end Behavioral;
```


```VHDL
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity shift_vector is
  Port ( sw : in STD_LOGIC_VECTOR (15 downto 0);
        clk : in STD_LOGIC;
        led : out STD_LOGIC_VECTOR (15 downto 0) );
end shift_vector;

architecture Behavioral of shift_vector is

signal temp : std_logic_vector(15 downto 0) := "0000000000000001";
signal sens : std_logic:='0';

begin

process (clk) 
    
    variable aux : std_logic := '1';
    variable varSens : std_logic := sens;
    begin   
    
    if (clk'event and clk='1') then
        if (temp(0) = '1' or temp(15)='1') then
            varSens := not varSens;   
        else
            for i in 0 to 15 loop 
              if (sw(i) = '1' and temp(i)='1') then
                  varSens := not varSens;
              end if;           
            end loop;     
        end if;
        if (varSens='1') then
                aux := temp(15);
                temp(15 downto 1) <= temp(14 downto 0);
                temp(0) <= aux;    
        else 
                aux := temp(0);
                temp(14 downto 0)<=temp(15 downto 1);
                temp(15) <= aux;
        end if;
     end if;
     sens <= varSens;            
end process;

led <= temp;

end Behavioral;
```

Certains processus peuvent utiliser une clock. 
Pour cela, on utilisera la clock les top d'horloge avec l'instruction suivant :

```
if (clk'event and clk='1') then
```

Il est possible de la rallentir, c'est pour cela que nous avons ajouté clk_div qui réduit 
la vitesse de la clock.

### Les FSM

```
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity controlcola is
 Port (    piece20, piece50, piece100 : in STD_LOGIC;
           reset : in std_logic;
           clk : in STD_LOGIC;
           euro_in : out STD_LOGIC_VECTOR (15 downto 0);
           canette, retour_euro : out STD_LOGIC);
end controlcola;

architecture Behavioral of controlcola is
    type state_type is (v0, v20, v40, v50, v60, v80, v100, vRetour);
    signal state, next_state : state_type;
    signal euro_in_i : STD_LOGIC_VECTOR (15 downto 0);
    signal canette_i : STD_LOGIC;
    signal retour_euro_i : STD_LOGIC;
begin

   SYNC_PROC: process (clk)
   begin
      if rising_edge(clk) then
         if (reset = '1') then
            state <= v0;
            canette <= '0';
            retour_euro <= '0';
            euro_in <= "0000000000000000";
         else
            state <= next_state;
            canette <= canette_i;
            retour_euro <= retour_euro_i;
            euro_in <= euro_in_i;
         end if;
      end if;
   end process;

   OUTPUT_DECODE: process (state)
   begin
      case state is
        when v0 => 
            canette_i <= '0';
            retour_euro_i <= '0';
            euro_in_i <= "0000000000000000";
        when v20 => 
            canette_i <= '0';
            retour_euro_i <= '0';
            euro_in_i <= "0000000000100000";
        when v40 => 
            canette_i <= '0';
            retour_euro_i <= '0';
            euro_in_i <= "0000000001000000";
        when v50 => 
            canette_i <= '0';
            retour_euro_i <= '0';
            euro_in_i <= "0000000001010000";
        when v60 => 
            canette_i <= '0';
            retour_euro_i <= '0';
            euro_in_i <= "0000000001100000";
        when v80 => 
            canette_i <= '0';
            retour_euro_i <= '0';
            euro_in_i <= "0000000010000000";
        when v100 => 
            canette_i <= '1';
            retour_euro_i <= '0';
            euro_in_i <= "0000000100000000";
        when vRetour => 
            canette_i <= '0';
            retour_euro_i <= '1';  
            euro_in_i <= "0000000000000000";      
        when others =>
            canette_i <= '0';
            retour_euro_i <= '0'; 
            euro_in_i <= "0000000000000000";   
      end case;
   end process;

   NEXT_STATE_DECODE: process (state, piece20, piece50, piece100)
   begin
      next_state <= state;
        case (state) is
           when v0 =>
              if(piece20 = '1') then                
                 next_state <= v20;
              elsif(piece50 = '1') then                
                 next_state <= v50;
              elsif(piece100 = '1') then           
                 next_state <= v100;                 
              end if;
           when v20 =>
              if(piece20 = '1') then                
                 next_state <= v40;
              elsif(piece50 = '1' or piece100 = '1') then               
                 next_state <= vRetour;           
              end if;
           when v40 =>
              if(piece20 = '1') then                
                 next_state <= v60;
              elsif(piece50 = '1' or piece100 = '1') then             
                 next_state <= vRetour;               
              end if;
           when v50 =>
              if(piece50 = '1') then               
                 next_state <= v100;
              elsif(piece20 = '1' or piece100 = '1') then                                 
                 next_state <= vRetour;                 
              end if;
           when v60 =>
              if(piece20 = '1') then                
                 next_state <= v80;
              elsif(piece50 = '1' or piece100 = '1') then               
                 next_state <= vRetour;                
              end if;
           when v80 =>
              if(piece20 = '1') then                
                 next_state <= v100;
              elsif(piece50 = '1' or piece100 = '1') then            
                 next_state <= vRetour;                
              end if;
           when v100 =>       
              if(piece20 = '1' or piece50 = '1' or piece100 = '1') then            
                 next_state <= vRetour;        
              else
                 next_state <= v100;            
              end if;
           when vRetour =>
              next_state <= vRetour;
           when others =>
              next_state <= v0;
        end case;
   end process;
end Behavioral;

```

Les FSM sont des machines à états. Dans cet exemple, nous avons programmé un
distributeur de cola.

Une FSM a trois états :

- `SYNC_PROC` permet de synchroniser le processus avec la clock.
- OUTPUT_DECODE permet de gérer l'output en fonction de l'état dans lequel on se trouve.
- NEXT_STATE_DECODE change l'état suivant d'après des critères propres au programme.

### HMD

Les fichiers HDM sont des programmes binaires qui sont a executer 
sur la carte direcctement.

```
0c00 0000 000c ffff 201f a402 8804 8003
a002 03fb 1400 ffff 1000 0000 0004 1c00
ffff ffff ffff ffff 1000 0000 0004 1c00
1400 ffff ffff ffff ffff ffff ffff ffff
```

##### Explications

x"0C00_0000_000C_FFFF", -- 0000
branchement absolu des adresses 0000 et 000c	 

x"201F_A402_8804_8003", -- 0004
201F : LIT 1F dans la pile instruction 
A402 : masque des boutons en allant chercher dans la pile 1F et dépile
8804 :  empile la valeur des switchs
8003 : envoie la valeur de la pile vers les leds mais ne dépile pas

x"A002_03FB_1400_FFFF", -- 0008
A002 dépile et envoie la valeur sur les segments
03FB Branchement sur -5 -> 201F (boucle)
1400 return

x"1000_0000_0004_1C00", -- 000C
CALL  0004

x"FFFF_FFFF_FFFF_FFFF", -- 0010

-- Slave code

x"1C00_FFFF_FFFF_FFFF", -- 0000
HALT

x"FFFF_FFFF_FFFF_FFFF" -- 0004

Le FFFF correspond à "ne rien faire".


### FSH

Les programmes FSH sont la traduction en humain des codes HMD.
Ils sont à compiler puis à exécuter sur la carte également.

##### fibo

```
:IP fibo $AC03; //1010 1100 0000 0011 - AC03
slave
start
master

: main

	begin
	ticraz
	switch
	fibo

	tic
	
	7seg
	$1F
	btn

	7seg
	$1F
	btn
  again
;

start
main
endprogram

```

##### rdm

``` 
:IP rdm $8810;
slave
start
master
: carre
	$FFF
	and
	dup
	mul16
;
```

##### pi

```
: main
	$0
	>1

	$FFFE
	for
		rdm
		>0
		0>
		carre
		0>
		$C
		->
		carre
		add
		$FFE001
		<=
		if
			1>
			1
			add
			>1
		endif
	next
	1>
	7seg
	$1F
	btn
;
start
main

endprogram
```


### Basys3V6

Pour ajouter des programmes à la carte, nous avons utilisé une implémentation de la carte donnée,
à laquelle nous avons ajouté nos entités.

##### random.vhd

```
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity random is
 generic ( width : integer :=  32 ); 
port (
   clk , reset : in std_logic;
   enable : in std_logic;
   random_num : out std_logic_vector (width-1 downto 0)   --output vector            
   );
end random;

architecture Behavioral of random is
    signal r : std_logic_vector(31 downto 0) := x"80000000";
begin

    process (clk)
    begin
        if rising_edge(clk) then
                if(reset = '1') then
                    r <= x"80000000";
                else
                  if(enable = '1') then
                    r(30 downto 0) <= r(31 downto 1);
                    r(31) <= (((r(0) xor r(2)) xor r(3)) xor r(4));
                  end if;
                end if;
        end if;
    end process;
    
    random_num <= r;

end Behavioral;
```

##### IP.Rdm.vhd

```
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity IP_Rdm is
    GENERIC (Mycode : std_logic_vector(10 downto 0) := "00000010000"
); 
    port (
 clk , reset : in std_logic; 
 IPcode : in std_logic_vector (10 downto 0);
 Tout : out std_logic_vector (31 downto 0);
 Nout : out std_logic_vector (31 downto 0)
); 
end IP_Rdm;

architecture Behavioral of IP_Rdm is

component random
 generic ( width : integer :=  32 ); 
port (
   clk , reset : in std_logic;
   enable : in std_logic;
   random_num : out std_logic_vector (width-1 downto 0)   --output vector            
   );
end component;
    signal random_num : std_logic_vector (31 downto 0);
    signal enable : std_logic;
begin

enable <= '1' when IPcode(10 downto 0) = Mycode else '0';

inst_random: random 
		generic map (width=>32)
		PORT MAP(
		clk => clk,
		reset => reset,
		enable => enable,
		random_num => random_num
		);
		
 Tout <= "00000000000000000000"&random_num(23 downto 12)  when IPcode(10 downto 0) = Mycode else (others =>'Z');
 Nout <= "00000000000000000000"&random_num(11 downto 0) when IPcode(10 downto 0) = Mycode else (others =>'Z');


end Behavioral;
```

##### Dans le code de IP_code.vhd

```
---- IP RDM  F
constant GenM_rdm : std_logic :='1';
constant GenS_rdm : std_logic :='1';
constant IPRdm 		: code := "00000010000";  --  0  1

```

##### Dans le code de Hmaster.vhd

```
COMPONENT IP_Rdm
	GENERIC (Mycode : std_logic_vector (10 downto 0));
	PORT(
		clk , reset: IN std_logic;
		IPcode : IN std_logic_vector(10 downto 0);          
		Tout : OUT std_logic_vector(31 downto 0);        
        Nout : OUT std_logic_vector(31 downto 0)
		);
	END COMPONENT;
```

```
Mrdm : if genM_rdm = '1' generate
Inst_IP_Rdm: IP_Rdm 
		generic map (Mycode =>IPRdm)
		PORT MAP(
		clk => clock,
		reset => reset,
		IPcode => Icode,
		Tout => Tbusst,
        Nout => Nbusst
	);
end generate Mrdm;
```
